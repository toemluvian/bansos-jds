# JDS Aplikasi Bansos

## Informasi Peserta

- Peserta Pelatihan JCC Batch 2
- Kelas : Vue JS
- Nama : M Luvian Tumbelaka

## Penjelasan

Sebelumnya mohon maaf tidak ada link demo karena laptop saya harus diservice selama 2 mingguan lebih dan alhasil pekerjaan saya tidak maksimal dan tidak saya dokumentasikan itu memang kesalahan saya. Kemudian saya mencoba mengerjakan mencoba menggunakan laptop teman alhasil harus di install semua tools yang diperlukan dari awal dan saya kebingungan terdapat error. Untuk itu mohon maaf saya tidak mencantumkan link demo ataupun deploy. Terima kasih atas pertimbangannya :)

## Tools

- Nuxt 3
- Tailwind CLI using PostCSS
- open for http://localhost:3000
- Vue.JS

Dibangun menggunakan [Nuxtjs 3 Beta](https://v3.nuxtjs.org).